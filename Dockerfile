FROM node:18-alpine3.17 as builder
ARG version
WORKDIR /app
COPY .  .
RUN npm ci --cache .npm --prefer-offline
RUN npm run build
RUN echo ${version} > ./build/version.html

FROM nginx:1.23.3-alpine
COPY --from=builder /app/build /usr/share/nginx/html
EXPOSE 80
